browser.browserAction.onClicked.addListener(divideBeforeTab);
browser.commands.onCommand.addListener(onDivideCommand);

function onDivideCommand() {
  let gettingCurrent = browser.tabs.query(
    {
      active: true, 
      currentWindow: true
    }
  );

  gettingCurrent.then(
    tabs => divideBeforeTab(tabs[0]), 
    logError
  );
}

function divideBeforeTab(tab) {
  console.log('current tab: ', tab);
  console.log('current index: ', tab.index);
  browser.tabs.create({
    url: '/division/index.html',
    index: tab.index
  });
}

function logError(error) {
  console.error(`Error: ${error}`);
  console.trace();
}

