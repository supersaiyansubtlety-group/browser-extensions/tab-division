let supervisedIds = new Set();

let thisData = {
  id: null,
  windowId: null,
  index: null
};

let refreshState = {
  coolingDown: false,
  pendingRefresh: false
};

init();

function init() {
  initState();
  window.addEventListener('visibilitychange', () => refresh());
  
  document.getElementById('store_supervised_tabs_button').addEventListener('click', storeSupervisedTabs);
  document.getElementById('restore_stored_tabs_button').addEventListener('click', restoreStoredTabs);
  
  let gettingCurrent = browser.tabs.getCurrent();
  gettingCurrent.then(
    currentTab => {
      // set id here because it will never change
      thisData.id = currentTab.id;
      refresh(currentTab);
      browser.tabs.onMoved.addListener(superviseMove);
      browser.tabs.onUpdated.addListener(superviseUpdate);
      browser.tabs.onRemoved.addListener(superviseRemoval);

      window.addEventListener('beforeunload', () => {
        supervisedIds.clear();
        browser.tabs.onMoved.removeListener(superviseMove);
        browser.tabs.onUpdated.removeListener(superviseUpdate);
        browser.tabs.onRemoved.removeListener(superviseRemoval);
      });
    }, 
    logError
  );
}

function refresh(currentTab) {
  if (refreshState.coolingDown) {
    refreshState.pendingRefresh = true;
  } else {
    refreshState.pendingRefresh = false;
    refreshState.coolingDown = true;
    
    window.setTimeout(
      () => {
        refreshState.coolingDown = false;
        if (refreshState.pendingRefresh) refresh();
      },
      250
    );
    
    if (document.visibilityState !== 'visible') return;
    supervisedIds.clear();
    lazyConsumeCurrentTab(
      currentTab, 
      _currentTab => {
        refreshGlobalProps(_currentTab);
        refreshLists(_currentTab);
      }
    );
  }
}

function refreshGlobalProps(currentTab) {
  // set these here because they can change
  if (document.visibilityState !== 'visible') return;
  lazyConsumeCurrentTab(
    currentTab, 
    _currentTab => {
      thisData.index = _currentTab.index;
      thisData.windowId = _currentTab.windowId;
    }
  );
}

function lazyConsumeCurrentTab(currentTab, tabConsumer) {
  if (currentTab) tabConsumer(currentTab);
  else {
    let gettingCurrent = browser.tabs.getCurrent();
    gettingCurrent.then(tabConsumer, logError);
  }
}

function refreshLists(currentTab) {
  let querying = browser.tabs.query({currentWindow: true});
  querying.then(
    tabs => populateLists(extractSupervisedTabs(tabs, currentTab)), 
    logError
  );
}

function populateLists(supervisedTabs) {
  populateTabsList(
    supervisedTabs, 
    'supervised_tabs', 
    (tab, tabLI) => {
      addTabLIData(tab, tabLI);
      supervisedIds.add(tab.id);
      tabLI = prependStoreButton(tabLI);
    }
  );
  populateTabsList(
    history.state.storedTabs, 
    'stored_tabs', 
    (tab, tabLI) => appendRestoreButton(tabLI)
  );
}

function populateTabsList(tabs, listId, LIConsumer) {
  let tabsUL = document.getElementById(listId);
  tabsUL.innerHTML = '';
  
  for (let tab of tabs) {
    var tabLI = document.createElement('li');
    tabLI.innerText = tab.title;
    LIConsumer(tab, tabLI);
    tabsUL.appendChild(tabLI);
  }
}

function prependStoreButton(element) {
  let textButton = document.createElement('button');
  textButton.addEventListener('click', onClickStoreSingle);
  textButton.type = 'button';
  textButton.classList.add('text_button', 'store_button');
  element.prepend(textButton);
}

function appendRestoreButton(element) {
  let textButton = document.createElement('button');
  textButton.addEventListener('click', onClickRestoreSingle);
  textButton.type = 'button';
  textButton.classList.add('text_button', 'restore_button');
  element.append(textButton);
}

function addTabLIData(tab, tabLI) {
  tabLI.id = tabIdToTabLIId(tab.id);
  tabLI.setAttribute('loading', isTabLoading(tab));
}

function superviseMove(id, info) {
  if (document.visibilityState !== 'visible') return;
  if (id === thisData.id) refresh();
  else if (thisData.windowId === info.windowId) {
    var numSupervised = document.getElementById('supervised_tabs').querySelectorAll('li').length;
    let afterLastSupervisedIndex = thisData.index + numSupervised;
    
    if (info.fromIndex >= afterLastSupervisedIndex) {
      if (info.toIndex < afterLastSupervisedIndex) {
        if (info.toIndex < thisData.index) {
          // far-right to far left, only index changed
          refreshGlobalProps();
        } else {
          // far-right to supervised
          supervisedIds.add(id);
          refresh();
        }
      }
    } else {
      if (info.fromIndex < thisData.index) {
        if (info.toIndex > thisData.index) {
          if (info.toIndex >= afterLastSpervisedIndex) {
            // far-left to far-right, only index changed
            refreshGlobalProps();
          } else {
            // far-left to supervised
            supervisedIds.add(id);
            refresh();
          }
        }
      } else {
        // from supervised
        if (info.toIndex >= afterLastSpervisedIndex || info.toIndex < thisData.index) {
          // from supervised to unsupervised
          supervisedIds.remove(id);
        }

        refresh();
      }
    }
  }
}

function superviseUpdate(id, change, tab) {
  if (!supervisedIds.has(id)) return;
  if (document.visibilityState !== 'visible') return;
  let tabLI = document.getElementById(tabIdToTabLIId(id));
  
  if (change.title) setFirstTextChild(tabLI, change.title);
  if (change.status) tabLI.setAttribute('loading', isTabLoading(tab));
}

function superviseRemoval(id, info) {
  if (supervisedIds.has(id)) {
    supervisedIds.delete(id);
    refresh();
  } else refreshGlobalProps();
}

function setFirstTextChild(element, content) {
  for (let child of element.childNodes) {
    if (child.nodeType === Node.TEXT_NODE) {
      child.nodeValue = content;
      return;
    }
  }
  console.error('Could not find text node!');
  console.trace();
}

function extractSupervisedTabs(windowTabs, currentTab) {
  const supervisedTabs = [];
  for (let tab of windowTabs) {
    if (tab.index <= currentTab.index) continue;
    if (isDivisionURL(tab.url)) break;
    supervisedTabs.push(tab);
  }
  return supervisedTabs;
}

function storeSupervisedTabs() {
  supervisedIds.clear();
  let gettingCurrent = browser.tabs.getCurrent();
  gettingCurrent.then(startStoring, logError);
}

function startStoring(currentTab) {
  let index = currentTab.index + 1;
  consumeTabAtIndex(index, tab => ifSupervisedStoreThenNext(tab, index));
}

function ifSupervisedStoreThenNext(tab, index) {
  if (tab && !isDivisionURL(tab.url)) {
    updateState([tabToSimple(tab)]);
    let removing = browser.tabs.remove(tab.id);
    // recur after removing
    removing.then(() => consumeTabAtIndex(index, tab => ifSupervisedStoreThenNext(tab, index)));
  } else refresh();
}

function consumeTabAtIndex(index, tabConsumer) {
  let gettingNextTab = browser.tabs.query({currentWindow: true, index: index});
  gettingNextTab.then(tabs => tabConsumer(tabs[0]), logError);
}

function restoreStoredTabs() {
  if (history.state && history.state.storedTabs && history.state.storedTabs.length) {
    let gettingCurrent = browser.tabs.getCurrent();
    gettingCurrent.then(tab => restoreNextToIndex(tab.index + 1), logError);
  }
}

function restoreNextToIndex(index) {
  if (history.state.storedTabs.length) {
    let restoringTab = history.state.storedTabs.shift();
    let creating = createTab(restoringTab.url, index, false);
    creating.then(tab => restoreNextToIndex(index + 1), logError);
  } else {
    clearState();
    refresh();
  }
}

function onClickStoreSingle(clickData) {
  let id = tabLIIdToTabId(clickData.target.closest('li').id);
  let gettingTab = browser.tabs.get(id);
  gettingTab.then(
    tab => {
      updateState([tabToSimple(tab)]);

      let removing = browser.tabs.remove(id);
      removing.then(refresh, logError);
    },
    logError
  );
}

function onClickRestoreSingle(clickData) {
  let clickedLI = clickData.target.closest('li');
  let tabLIs = clickedLI.parentElement.querySelectorAll(':scope > li');
  for (let i = 0; i < tabLIs.length; i++) {
    let tabLI = tabLIs[i];
    if (tabLI === clickedLI) {
      let url = history.state.storedTabs.splice(i, 1)[0].url;
      let creating = createTab(url, thisData.index + 1, false);
      creating.then(
        () => {
          updateState();
          refresh();
        }, 
        logError
      );
      return;
    }
  }
  logError('Could not get index of <li> element');
}

function createTab(url, index, active) {
  url = getSafeUrl(url);
  return browser.tabs.create({
    url: url,
    index: index,
    active: false
  });
}

function getSafeUrl(url) {
  let safeURL = new URL(url);
//  console.log('about:newtab URl: ', new URL('about:newtab'));
  switch (safeURL.protocol) {
    case 'about:': {
      switch (safeURL.pathname) {
        case ('newtab'): 
          return undefined;
        case ('blank'): return url;
      }
    }
    case 'chrome:':
    case 'javascript:':
    case 'data:':
    case 'file:':
      return '/placeholder/index.html?page=' + url;
  }
  return url;
}

function updateState(simpleTabs) {
  let newState = history.state;
  if (simpleTabs) newState.storedTabs = newState.storedTabs.concat(simpleTabs);
  history.replaceState(newState, "");
}

function initState() {
  if (!history.state) clearState();
  else {
    let changed = false;
    if (!history.state.storedTabs) {
      history.state.storedTabs = [];
      changed = true;
    }
    
    if (changed) history.replaceState(history.state, '');
  }
}

function clearState() {
  history.replaceState({storedTabs: [] }, '');
}

function isDivisionURL(url) {
  return new URL(url).pathname === '/division/index.html';
}

function tabIdToTabLIId(id) {
  return 'tab_id::' + id;
}

function tabLIIdToTabId(tabLIId) {
  return parseInt(tabLIId.replace('tab_id::', ''));
}

function isTabLoading(tab) {
  return tab.status === 'loading';
}

function tabToSimple(tab) {
  return { 
    title: tab.title, 
    url: tab.url
  }
}

function logError(error) {
  console.error(`Error: ${error}`);
  console.trace();
}